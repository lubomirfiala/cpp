#include "rectangle.h"

#ifndef rectComparsion_h
    #define rectComparsion_h

    int fCompareArea(sRectangle &rx, sRectangle &ry);
    int fComparePerimeter(sRectangle &rx, sRectangle &ry);

#endif
