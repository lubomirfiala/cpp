#include "rectangle.h"

#ifndef rectOperations_h
    #define rectOperations_h

    int fRotateRectangleRight (sRectangle & rx);
    int fAreaRectangle (sRectangle & rx );
    int fPerimeterRectangle (sRectangle *px);

#endif
