#include <iostream>
#include "rectComparsion.h"
using namespace std;


int fCompareArea(sRectangle &rx, sRectangle &ry) {


    if( rx.area > ry.area ) {
        return 0;
    } else if ( rx.area < ry.area ) {
        return 1;
    } else {
        return 2;
    }

}

int fComparePerimeter(sRectangle &rx, sRectangle &ry) {


    if( rx.perimeter > ry.perimeter ) {
        return 0;
    } else if ( rx.perimeter < ry.perimeter ) {
        return 1;
    } else {
        return 2;
    }

}

