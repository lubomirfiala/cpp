#include <iostream>
#include "rectOperations.h"
#include "rectComparsion.h"
using namespace std;



int main() {
    cout << "Mereni obdelnika \n" ;

    sRectangle x { "A", 1200, 628, 0 ,0 };
    sRectangle y { "B", 2400, 314 ,0 ,0 };




    fRotateRectangleRight(x);

    fAreaRectangle(x);
    fAreaRectangle(y);

//    cout << "area of rectangle Y \t\t" << y.area << "\n";

    fPerimeterRectangle(&x);
    fPerimeterRectangle(&y);
//    cout << "perimeter of rectangle X \t" << x.perimeter << endl;
//    cout << "perimeter of rectangle Y \t" << y.perimeter << endl;

    cout << "area comparsion: \t\t\t";

    switch ( fCompareArea(x, y) ) {
        case 0 : cout << "fist one is the winner" << endl; break;
        case 1 : cout << "second one is the winner" << endl; break;
        case 2 : cout << "both rectangles are equal" << endl; break;
        default: cout << "Houston, we got a problem up here" << endl;
    }


    cout << "perimeter comparsion: \t\t";

    switch ( fComparePerimeter(x,y) ) {
        case 0 : cout << "fist one is the winner" << endl; break;
        case 1 : cout << "second one is the winner" << endl; break;
        case 2 : cout << "both rectangles are equal" << endl; break;
        default: cout << "Houston, we got a problem up here" << endl;
    }

    sRectangle s[10] {
        { "OCISLY", 2631, 994, 0 ,0 },  // Of Course I Still Love You
        { "JRTI  ", 2332, 234, 0 ,0 },  // Just Read The Instructions
        { "ASOG  ", 4311, 793, 0 ,0 },  // A Shortfall of Gravitas
        { "ZG    ", 3432, 907, 0 ,0 },  // Zero Gravitas
        { "DaG   ", 1512, 235, 0 ,0 },  // Death and Gravity
        { "VLGI  ", 5432, 443, 0 ,0 },  // Very Little Gravitas Indeed
        { "UCOE  ", 1235, 124, 0 ,0 },  // Unfortunate Conflict Of Evidence
        { "ZLOBOT", 1123, 556, 0 ,0 },  // Zlínský Robot
        { "TEOI  ", 1134, 421, 0 ,0 },  // The Ends Of Invention
        { "OSB   ", 5543, 123, 0 ,0 },  // Only Slightly Bent
    };

    int biggestAreaId = 0;
    int size = sizeof(s)/sizeof(*s);
    for (int i = 0; i < size; i++ ) {
        fAreaRectangle (s[i] );
        if (s[biggestAreaId].area <= s[i].area) {
            biggestAreaId = i;
        }
    }

    cout << "biggest ship in fleet is: " << s[biggestAreaId].name << endl;

    cout << "end of program" << endl;



    return 0;



}