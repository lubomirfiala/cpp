#include <iostream>


using namespace std;

typedef struct {
    char name[8];
    int startNumber;
    int time;
} sCars ;


int main() {

    sCars cars[10] {
        { "OCISLY", 1, 4640 },  // Of Course I Still Love You
        { "JRTI  ", 2, 4522 },  // Just Read The Instructions
        { "ASOG  ", 3, 4728 },  // A Shortfall of Gravitas
        { "ZG    ", 4, 4680 },  // Zero Gravitas
        { "DaG   ", 5, 4855 },  // Death and Gravity
        { "VLGI  ", 6, 4970 },  // Very Little Gravitas Indeed
        { "UCOE  ", 7, 4760 },  // Unfortunate Conflict Of Evidence
        { "ZLOBOT", 8, 4521 },  // Zlínský Robot
        { "TEOI  ", 9, 4579 },  // The Ends Of Invention
        { "OSB   ", 10, 4637 },  // Only Slightly Bent
    };

    int winner = 0, minTime = cars[0].time, looser = 0, maxTime = 0,sumOfTime = 0, i = 0;

    for (auto &car : cars) {

        cout << car.name << "\t no. " << car.startNumber << "\t time: " << car.time << "s" << endl;

        if (car.time < minTime) {
            minTime = car.time;
            winner = i;
        }

        if (car.time > maxTime) {
            maxTime = car.time;
            looser = i;
        }

        sumOfTime += car.time;
        i++;
    }
    int avgTime = sumOfTime / 10;

    cout << "and the winner is: " << cars[winner].name << endl;
    cout << "and the looser is: " << cars[looser].name << endl;
    cout << "avereage time is: " << avgTime << "s" <<  endl;


}










